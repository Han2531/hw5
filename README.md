# Homework 5

### Due Wed 2/24/21 @ 12a





##### 2. [5 pts] What byte does the PinePhone's CPU look at for bootloader bytecode??
- It looks at 8KiB offset from the start of the block device for the contents of p-boot.bin


##### 5. Explore the Official Linux kernel

[5 pts] What is version the most recent kernel release?  
- 5.11 mainline

[5 pts] What version is the latest "stable" kernel release?
- 5.11.1 stable


##### 6. Explore Pine64's Linux kernel


Pine64, the group that designed and manufactured our phones, maintains their own version of the Linux Kernel.  Their source code is hosted at https://gitlab.com/pine64-org/linux .
Visit that page and see what branches of their project are active.
(go to "Repository - > Branches" in the left nav. pane)

- > [5 pts] What is the name of the most active branch?
- pine64-kernel-ubports-5.10.y-megi
- > [3 pts] What version of the Linux kernel do you think that branch is based on?
- 5.10.y
- > [3 pts] What is the most recent commit made to that branch?
- Merge remote-tracking branch 'stable/linux-5.7.y' into pine64-kernel-5.7.y
- > [3 pts] Who authored that commit?
- Samuel Holland authored 5 months ago
- > [3 pts] When did they author the commit?
- 03 Sep, 2020
- > [3 pts] Who authored the commit with the hash `9d640944`?  
- Ondrej Jirman
- > [5 pts] Does that person go by another name?
- Ondřej Jirman or Megi


##### 7. Explore Megi's Linux kernel

Megi also maintains his own versions of the Linux kernel.  Visit his page at 
https://megous.com/git/linux/.

- > [3pts] Which branch of his repo do you think he uses for the PinePhone?
- pp
- > [3pts] What version of the kernel is that based on?
- 5.12
- > [3pts] When was Megi's latest commit to the pp-5.11 branch?
- 2021-02-15 07:49:39
- > [3pts] What file was modified in that commit?
- arm64: dts: sun50i-pinephone: Add interrupt pin for WiFi


##### 8. Download a Pre-Built copy of Megi's Linux kernel


>[3 pts] Try `curl www.google.com` (screenshot) and see what comes back.  What kind of file is that?
-HTML
>[3 pts] This time, fetch megi's key but redirect stdout to a file (save in repo) called 'megi.key':  `curl <url-here>   >  megi.key`

This time, fetch the key, and redirect it directly into your GPG keyring:
`curl <paste-link-here> | gpg --import -`

The vertical line in the last command is called a "pipe."  It is used to pipe the output of the first command (stdout) into the next command's stdin.  Just like plumbing.  The '-' tells
gpg to import from stdin.

>[3 pts] Now you can verify the file: `gpg --verify pp.tar.gz.asc pp.tar.gz`(screenshot) be sure you see "Good signature from Ondrej Jirman...."

Extract the tarball that we just verified: `tar -xf <filename-goes-here>`.
You can now delete the .gz and .gz.asc files if you like.

##### 9. Setup a build environment to make LuneOS:

https://www.webos-ports.org/wiki/Build_for_Pinephone details how the LuneOS image is built for the pinephone.

We are going to follow the directions there (maybe with a few modifications).

1) All POSIX-compliant OSes provide a program located at `/bin/sh` which behaves as a "shell."
A shell is a basic command line input/output system.  By default, in Ubuntu, /bin/sh just points to `/bin/dash` which is a very very simple and inflexible shell. All the Lune build scripts are written for bash, even though some are invoked with `/bin/sh`.  Therefore, we want `/bin/sh` to point to `/bin/bash` instead:
```
sudo dpkg-reconfigure dash
```

2) There is a typo in the webOS instructions. We want version 14, not 4:

```
$ curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
```
Then, as the output of the previous command says, install nodejs: 
```
sudo apt install -y nodejs
```

3) Use `apt` to install all the other tools we'll need:

```
gawk wget git-core diffstat unzip texinfo build-essential chrpath libsdl1.2-dev xterm bzr
```

4) Setup a build-environment for LuneOS (a webOS port):

```bash
cd into/your/build-directory
mkdir webos-ports-env && cd webos-ports-env
```

5) Create a variable in the shell; it will be exported into the environments of all child processes:

```
export ENV_NAME=testing
```
The value of the variable can be dereferenced by using `$` and optionally enclosing the name in braces: `${ENV_NAME}`.

> [3pts ] Print the value of the variable to the terminal with the `echo` command:
`echo ${ENV_NAME}` (screenshot: echo.png)


6) Get the makefile from the webos github server:

```
wget https://raw.github.com/webOS-ports/webos-ports-setup/$ENV_NAME/Makefile
```

7) Run the 'setup-webos-ports' section of the makefile with:

```
make setup-webos-ports
```

##### 10. Build LuneOS:

1) make sure you're in the `webos-ports-env` directory

2) Source the 'setup-env' file found at `webos-ports/setup-env` by running the command `source webos-ports/setup-env`  (note that the webOS-ports webpage uses the `.` command instead of `source` -- these are identical commands.  `.` is just an alias for `source`).

When you 'source' a file, the shell will read and execute the contents of that file line-by-line just as though they were typed into the shell on the command line.

After sourcing the file, you should see your prompt change from something like `user@machine-name:` to `OE @luneos`

> [10pts] Take a screenshot of your old and new bash prompts (prompts.png)

3) run the 'update' portion of the makefile: `make update`

4) cd back into the 'webos-ports' directory

5) simultaneously set the environment variable 'MACHINE' to 'pinephone' immediately before calling `bb` to start the build:

```
MACHINE=pinephone bb luneos-dev-package
```

6) Sit back and watch the fireworks... for a long time.  If you need to stop the build, press <ctl>+c and wait for BB to clean up.  You can resume the build by repeating steps 1)-5) above.

> [10pts] Wait for BitBake to start "executing tasks..." and then take a screenshot of BitBake running (bitbake.png) 
